import random
import numpy as np
from time import time
from collections import Counter

class Clustering():
    def __init__(self, d, m, nbMot, nbCent):
        self.d, self.m, self.nbMot = d, m, nbMot
        
        self.t1 = time()
        self.cents = [0]*nbCent
        self.closestCent, self.pastCC = {}, {}
        self.iteration = 0

        #Select random starting centroïds
        for i in range(nbCent):
            temp = self.m[random.randrange(0, len(self.m))]
            self.cents[i] = temp

        #loop until past closest centroïds dictionarry is the same as current
        while True:
            t2 = time()

            self.clust()

            print("Itération " + str(self.iteration))
            diffs = len(self.closestCent) - len(self.closestCent.items() & self.pastCC.items())
            print(diffs, f'changements de cluster en {time() - t2} secondes.\n\n')
            for i in range(len(self.cents)):
                print("Il y a " + str(sum(val == i for val in self.closestCent.values())) + " points (mots) regroupés autour du centroïd " + str(i))
            print("\n============================================================")

            if (self.pastCC == self.closestCent):
                break
            else:
                self.pastCC.clear()
                self.pastCC.update(self.closestCent)
                
                #if not the same, center centroïds
                for i in range(nbCent):
                    temp = [0]*len(self.m)
                    count = 1
                    for j in range(len(self.m)):
                        if (self.closestCent[j] == i):
                            temp += self.m[j]
                            count += 1
                    self.cents[i] = temp/count
                self.iteration += 1
        
        self.showFinish()

    def clust(self):
        for i in range(len(self.m)):
            temp, preVal, tempCC = 0, 0, 0
            for j in range(len(self.cents)):
                temp = np.sum((self.m[i] - self.cents[j])**2)
                if(temp < preVal or preVal == 0):
                    preVal = temp
                    tempCC = j
            self.closestCent[i] = tempCC

    def showFinish(self):
        for i in range(len(self.cents)): 
            print("Groupe", i, "\n")
            score = {}
            
            for mot, index in self.d.items():
                if (self.closestCent[index] == i):
                    score[mot] = (np.sum((self.m[index]-self.cents[i])**2))
            
            for mot, score in sorted(score.items(), key=lambda item: item[1], reverse = True)[:self.nbMot]:
                print(mot, "-->", score)
            print("\n************************************************************\n")
        
        print("Clustering en", self.iteration, "itérations. Temps écoulé :", time() - self.t1, "secondes")
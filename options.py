from sys import argv, exit
import argparse

class Options:
    pass

def creer_parseur():
    p = argparse.ArgumentParser()
    
    p.add_argument('-e', dest='entrainement', action='store_true')
    p.add_argument('-r', dest='recherche', action='store_true')
    p.add_argument('-c', dest='cluster', action='store_true')
    p.add_argument('-b', dest='bd', action='store_true')
    p.add_argument('-t', dest='fenetre', type=int)
    p.add_argument('-n', dest='nbMots', type=int)
    p.add_argument('-k', dest='nbCent', type=int)
    p.add_argument('--enc', dest='encodage', type=str)
    p.add_argument('--chemin', dest='chemin', type=str)
    
    return p


def options(arguments):
    opt = Options()
    p = creer_parseur()
    p.parse_args(args=arguments, namespace=opt)
    
    if opt.entrainement + opt.recherche + opt.bd + opt.cluster != 1:
        raise Exception('Vous devez choisir une seule option entre entraînement et recherche.')
    
    if opt.bd:
        return opt
    
    if opt.fenetre is None or opt.fenetre < 3 or opt.fenetre % 2 == 0:
        raise Exception('La taille de la fenêtre doit être un nombre impair supérieur à 1.')
    
    if opt.entrainement:
        if opt.chemin == None or opt.encodage == None:
            raise Exception('Pour l\'entraînement, vous devez fournir un chemin et un encodage.')
        with open(opt.chemin, 'r', encoding=opt.encodage) as f:
            pass
    return opt
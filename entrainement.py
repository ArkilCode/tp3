import numpy as np
import re

EXP = '\w+'

#ceci devrait se nommer le Modele...
class Entrainement():
    def __init__(self, fenetre):
        self.fenetre = fenetre
        
    def entrainer(self, chemin, encodage):
        self.texte = self.parser(chemin, encodage)
        self.d = self.extraire_vocabulaire()
        self.texte = [self.d[mot] for mot in self.texte]
        self.dc = self.extraire_cooccurrences()
        self.m = self.remplir_matrice(self.d, self.dc)
        
    def parser(self, chemin, encodage):
        with open(chemin, 'r', encoding = encodage) as f:
            return re.findall(EXP, f.read().lower())
            
    def extraire_vocabulaire(self):
        d = {}
        for mot in self.texte:
            if mot not in d:
                d[mot] = len(d)
        return d
                
    #les mots sont des indexes
    def extraire_cooccurrences(self):
        dc = {}
        t2 = self.fenetre//2
        offsets = list(range(1, t2 + 1))
        
        for i in range(t2):
            ir = self.texte[i]
            for j in offsets:
                if i-j < 0:
                    break
                ic = self.texte[i-j]
                if ir <= ic:
                    coocc = (ir, ic)
                    if coocc not in dc:
                        dc[coocc] = 1
                    else:
                        dc[coocc] += 1
                if ic <= ir:
                    coocc = (ic, ir)
                    if coocc not in dc:
                        dc[coocc] = 1
                    else:
                        dc[coocc] += 1
                        
        for i in range(t2, len(self.texte)):
            ir = self.texte[i]
            for j in offsets:
                ic = self.texte[i-j]
                if ir <= ic:
                    coocc = (ir, ic)
                    if coocc not in dc:
                        dc[coocc] = 1
                    else:
                        dc[coocc] += 1
                if ic <= ir:
                    coocc = (ic, ir)
                    if coocc not in dc:
                        dc[coocc] = 1
                    else:
                        dc[coocc] += 1
                        
        return dc
                      
    def remplir_matrice(self, d, dc):
        m = np.zeros( (len(d), len(d)) )
        for (ir, ic), freq in dc.items():
            m[ir][ic] = freq
            m[ic][ir] = freq
        return m
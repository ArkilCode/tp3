from entrainement import Entrainement

class EntrainementBD(Entrainement):
    def __init__(self, fenetre, dao):
        super().__init__(fenetre)
        self.dao = dao
        
    def chercher_mots(self):
        return dict(self.dao.chercher_mots())
        
    def chercher_cooccurrences(self):
        d = {}
        for ir, ic, tfen, freq in self.dao.chercher_cooccurrences(self.fenetre):
            d[(ir, ic)] = freq
        return d
    
    def chargerBD(self):
        self.dBD = self.chercher_mots()
        self.dcBD = self.chercher_cooccurrences()
        
    def dico_matrice(self):
        m = super().remplir_matrice(self.dBD, self.dcBD)
        return self.dBD, m
        
    def maj_mots(self):
        nouveau_mots = []
        for mot in self.texte:
            if mot not in self.dBD:
                nouveau_mots.append( (mot, len(self.dBD)) )
                self.dBD[mot] = len(self.dBD)
                
        self.dao.inserer_mots(nouveau_mots)
                
    def maj_cooccurrences(self, dc):
        insert, update = [], []
        for (ir, ic), freq in dc.items():
            if (ir, ic) in self.dcBD:
                update.append( (freq + self.dcBD[(ir, ic)], ir, ic, self.fenetre) )
            else:
                insert.append( (ir, ic, self.fenetre, freq) )
                
        self.dao.inserer_cooccurrences(insert)
        self.dao.maj_cooccurrences(update)
    
    def entrainer(self, chemin, encodage):
        self.texte = self.parser(chemin, encodage)
        self.maj_mots()
        self.texte = [self.dBD[mot] for mot in self.texte]
        dc = self.extraire_cooccurrences()
        self.maj_cooccurrences(dc)
        
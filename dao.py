import sqlite3

CHEMINBD = 'cooccurrences.bd'

#ACTIVER_FK = 'PRAGMA foreign_keys = 1'
ACTIVER_FK = 'PRAGMA foreign_keys = ON'

CREER_MOT = '''
CREATE TABLE IF NOT EXISTS mot
(
    chaine  CHAR(15) NOT NULL,
    id      INT PRIMARY KEY NOT NULL
)
'''
DROP_MOT = 'DROP TABLE IF EXISTS mot'
SELECT_MOT = 'SELECT * FROM mot'
INSERT_MOT = 'INSERT INTO mot VALUES(?, ?)'

CREER_COOCCURRENCE = '''
CREATE TABLE IF NOT EXISTS cooccurrence
(
    id_mot          INT REFERENCES mot(id) NOT NULL,
    id_cooccurrence INT REFERENCES mot(id) NOT NULL,
    taille_fenetre  INT NOT NULL,
    frequence       INT NOT NULL,
    PRIMARY KEY(id_mot, id_cooccurrence, taille_fenetre)
)
'''
DROP_COOCCURRENCE = 'DROP TABLE IF EXISTS cooccurrence'
SELECT_COOCCURRENCE = 'SELECT * FROM cooccurrence WHERE taille_fenetre = ?'
INSERT_COOCCURRENCE = 'INSERT INTO cooccurrence VALUES(?, ?, ?, ?)'
UPDATE_COOCCURRENCE = 'UPDATE cooccurrence SET frequence = ? WHERE id_mot = ? AND id_cooccurrence = ? AND taille_fenetre = ?'

class Dao():
    def __init__(self):
        pass
        
    def connecter(self):
        self.connexion = sqlite3.connect(CHEMINBD)
        self.curseur = self.connexion.cursor()
        self.curseur.execute(ACTIVER_FK)
        
    def deconnecter(self):
        self.curseur.close()
        self.connexion.close()
        
    def creer_bd(self):
        self.curseur.execute(DROP_COOCCURRENCE)
        self.curseur.execute(DROP_MOT)
        self.curseur.execute(CREER_MOT)
        self.curseur.execute(CREER_COOCCURRENCE)
        
    def chercher_mots(self):
        self.curseur.execute(SELECT_MOT)
        return self.curseur.fetchall()
        
    def chercher_cooccurrences(self, fen):
        self.curseur.execute(SELECT_COOCCURRENCE, (fen,))
        return self.curseur.fetchall()
    
    def inserer_mots(self, mots):
        self.curseur.executemany(INSERT_MOT, mots)
        self.connexion.commit()
        
    def inserer_cooccurrences(self, cooccurrences):
        self.curseur.executemany(INSERT_COOCCURRENCE, cooccurrences)
        self.connexion.commit()
        
    def maj_cooccurrences(self, cooccurrences):
        self.curseur.executemany(UPDATE_COOCCURRENCE, cooccurrences)
        self.connexion.commit()
        
from sys import argv, exit
from traceback import print_exc
from time import time
from entrainementBD import EntrainementBD
from recherche import Recherche
from options import options
from dao import Dao
from clustering import Clustering


INVITE = '''
Entrez un mot, le nombre de synonymes que vous voulez et la méthode de calcul,
i.e. produit scalaire: 0, least-squares: 1, city-block: 2

Tapez q pour quitter.

'''
QUITTER = 'q'

def demander_mots(r):
    infos = input(INVITE)
    while infos != QUITTER:
        infos = infos.split()
        mot, nb, methode = infos[0], int(infos[1]), int(infos[2])
        print()
        t = time()
        r.chercher(mot, nb, methode)
        print(f'Recherche en {time() - t} secondes')
        infos = input(INVITE)

def main():
    try:
        opts = options(argv[1:])
        
        dao = Dao()
        dao.connecter()
        
        if opts.bd:
            dao.creer_bd()
        else:
            e = EntrainementBD(opts.fenetre, dao)
            e.chargerBD()
            if opts.entrainement:
                t = time()
                e.entrainer(opts.chemin, opts.encodage)
                print(f'Entrainement en {time() - t} secondes')
            elif opts.recherche:
                d, m = e.dico_matrice()
                r = Recherche(d, m)
                demander_mots(r)
            elif opts.cluster:
                e = EntrainementBD(opts.fenetre, dao)
                e.chargerBD()
                d, m = e.dico_matrice()
                c = Clustering(d, m, opts.nbMots, opts.nbCent)
                
        dao.deconnecter()
    except:
        print_exc()
        return 1
    return 0
    
if __name__ == '__main__':
    exit(main())
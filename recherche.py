import numpy as np

STOP = 'l le la les au aux un une du des c ça ce ces celle celui celles ceux je j tu il elle on nous vous ils elles me m te t se s y à de pour sans par mais ou et donc car ni or ne n pas dans que qui qu de d mon ma mes ton ta tes son sa ses notre nos votre vos leur leurs lui en quel quelle quelles lequel laquelle lesquels lesquelles dont quoi quand où comment pourquoi'

def least_squares(u, v):
    return np.sum((u-v)**2)
    
def city_block(u, v):
    return np.sum(np.abs(u-v))

class Recherche():
    def __init__(self, d, m):
        self.d, self.m = d, m
        self.stop = {}
        for mot in STOP.split():
            self.stop[mot] = 1
        
    def chercher(self, mot, nb, methode):
        if mot not in self.d:
            print(f'"{mot}" inconnu')
            return

        if methode == 0:
            f = np.dot
        elif methode == 1:
            f = least_squares
        elif methode == 2:
            f = city_block
        else:
            print('Méthode inconnue')
            return
            
        for score, mot in self.calculer(mot, nb, f):
            print(mot, '-->', score)
        
    def calculer(self, mot_r, nb, f):
        i_r = self.d[mot_r]
        scores = []
        for mot, i in self.d.items():
            if i != i_r and mot not in self.stop:
                scores.append( (f(self.m[i_r], self.m[i]), mot) )
        return sorted(scores, reverse = f == np.dot)[:nb]